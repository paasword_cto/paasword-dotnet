﻿# Paas-Word ASP.NET Middleware

[Paas-Word](https://www.paas-word.com) is an online authentication and user management service.
This ASP.NET middleware library by [Paas-Word](https://www.paas-word.com) enables website owners with a ASP.NET backend to
restrict their endpoints to authenticated users only and retrieve user data. 

## Usage

1. Create a free account at [Paas-Word](https://www.paas-word.com) website.
2. Recieve a login, sign-up, account and forgot-password pages for your website based on the user attributes you set up.
3. Set the callback pages on your website where users will be redirected after they sign-up and log in. 
4. Once a user is redirected to your website with a token, send this token to your backend in the "x-auth-token" header.

## Installation

Add the following to your pom.xml file

1. In Visual Studio, right click on your project
2. Add -> Add NuGet Packages...
3. Search for "Paasword" and click "Add Package"

## Set Private Key as Environment Variable
Create an app on [Paas-Word](https://www.paas-word.com) and then set its Private Key as an environment variable. 

In Visual Studio, Go to Run -> Run with -> Custom Configuration...
Click "Add" and add your App's Private Key

```c
Variable: PAASWORD_APP_PRIVATE_KEY
Value: 93f56f52-957d-4953-93a6-c5492e79778b
```

## Guard all endpoints
In Startup.cs

```c
using Paasword;

app.UsePaaswordMiddleware();

app.UseHttpsRedirection();
app.UseMvc();
```

## Guard specific routes against unauthenticated users
Add the routes dedicated to logged-in users. 

```c
app.MapWhen(context => context.Request.Path.Equals("/users/{id}") && 
      context.Request.Method.Equals(HttpMethods.Get), subApp =>
      {
           subApp.UsePaaswordMiddleware();
           subApp.Run(async (context) =>
                {
                    ....
                });
       });

```

## Retrieve user information

```c
app.MapWhen(context => context.Request.Path.Equals("/users/{id}") && 
      context.Request.Method.Equals(HttpMethods.Get), subApp =>
      {
           subApp.UsePaaswordMiddleware();
           subApp.Run(async (context) =>
                {
                    var user = context.User;
                    await context.Response.WriteAsync(user.ToString());
                    ....
                });
       });
```
