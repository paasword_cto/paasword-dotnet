﻿using System;
using Microsoft.AspNetCore.Builder;

namespace Paasword.Extensions
{
    public static class AuthMiddlewareExtension
    {
        public static IApplicationBuilder UsePaaswordMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<Paasword.AuthMiddleware>();
        }
    }
}
