﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.Extensions.Primitives;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace Paasword
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                if (!context.Request.Headers.ContainsKey("x-auth-token"))
                {
                    context.Response.StatusCode = 401;
                    context.Response.ContentType = "application/json; charset=utf-8";
                    string json = "{ \"ErrorType\": \"MISSING_ELEMENT\", \"ErrorMessage\": \"x-auth-token\" }";
                    await context.Response.WriteAsync(json);
                    return;
                }

                var appPrivateKey = Environment.GetEnvironmentVariable("PAASWORD_APP_PRIVATE_KEY");
                if (appPrivateKey == null) {
                    context.Response.StatusCode = 401;
                    context.Response.ContentType = "application/json; charset=utf-8";
                    string json = "{ \"ErrorType\": \"MISSING_ELEMENT\", \"ErrorMessage\": \"PAASWORD_APP_PRIVATE_KEY\" }";
                    await context.Response.WriteAsync(json);
                    return;
                }

                var accessToken = GetHeader(context.Request, "x-auth-token");

                var key = Encoding.ASCII.GetBytes(appPrivateKey); 
                var handler = new JwtSecurityTokenHandler(); 
                var tokenSecure = handler.ReadToken(accessToken) as SecurityToken;

                var validations = new TokenValidationParameters
                {
                    RequireExpirationTime = false,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                }; 
                var user = handler.ValidateToken(accessToken, validations, out tokenSecure);

                int limit = -1;
                long loginTime = -1;
                int hoursSinceLogin = -1;
                if (user.Identity.IsAuthenticated) {
                    foreach (var claim in user.Claims)
                    {
                        if (claim.Type == "AutoLogout")
                        {
                            var autoLogout = JObject.Parse(claim.Value);
                            if ((bool)autoLogout["IsEnabled"])
                            {
                                limit = (int)autoLogout["LogoutEveryXHours"];
                            }
                        }

                        if (claim.Type == "iat")
                        {
                            loginTime = Convert.ToInt32(claim.Value);
                        }
                    }

                    if (limit > -1 && loginTime > -1)
                    {
                        long now = DateTimeOffset.Now.ToUnixTimeSeconds();
                        hoursSinceLogin = (int)Math.Round((double)(now - loginTime) / 3600);
                    }
                    if (hoursSinceLogin > limit)
                    {
                        context.Response.StatusCode = 401;
                        context.Response.ContentType = "application/json; charset=utf-8";
                        string json = "{ \"ErrorType\": \"SESSION_EXPIRED\", \"ErrorMessage\": \"\" }";
                        await context.Response.WriteAsync(json);
                        return;
                    }
                    context.User = user;

                }
                await _next.Invoke(context);
            
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = 500;
                context.Response.ContentType = "application/json; charset=utf-8";
                string json = "{ \"ErrorType\": \"INTERNAL_ERROR\", \"ErrorMessage\": \"\" }";
                await context.Response.WriteAsync(json);
                return;
            }
        }

        public static string GetHeader(HttpRequest request, string key)
        {
            StringValues keys;
            if (!request.Headers.ContainsKey(key))
                return null;
            request.Headers.TryGetValue(key, out keys);
            return keys[0];
        }
    }

    public static class AuthMiddlewareExtension
    {
        public static IApplicationBuilder UsePaaswordMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthMiddleware>();
        }
    }
}
